Assignment Discussion Forum Built by Arshaan Shariff
====================================================

Key Features
------------

. Spring MVC framework
. Hibernate database layer
. Service and DAO layer API style architecture
. Annotation based query
. XSS filters
. Spring Security for Authentication of roles
. Email Validation
. Spring forms
. Responsive bootstrap framework integration
. File upload for user profile

