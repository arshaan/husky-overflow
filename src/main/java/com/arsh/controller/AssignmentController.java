package com.arsh.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.arsh.model.Assignment;
import com.arsh.model.User;
import com.arsh.service.AssignmentService;
import com.arsh.service.CourseService;
import com.arsh.service.UserService;

@Controller
public class AssignmentController {

	@Autowired
	AssignmentService assignmentService;
	
	@Autowired
	CourseService courseService;
	
	@Autowired
	UserService userService;

	// list assignments
	@RequestMapping(value = "admin/assignment/list", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("assignment", new Assignment());
		model.addAttribute("listAssignments",
				this.assignmentService.listAssignment());
		return "admin/assignment/list";
	}

	// ADMIN ROLE --> add assignment get
	@RequestMapping(value = "admin/assignment/add", method = RequestMethod.GET)
	public String addAssignmentByAdmin(Model model) {
		Assignment assignment = new Assignment();
		model.addAttribute("assignment", assignment);
		model.addAttribute("ListCourses", this.assignmentService.listCourses());
		return "admin/assignment/add";
	}
	
	// add assignment post
	@RequestMapping(value = "admin/assignment/add", method = RequestMethod.POST)
	public String processAdd( @ModelAttribute("assignment") Assignment assignment) {
		this.assignmentService.saveAssignment(assignment);;
		return "redirect:/admin/assignment/list";
	}
	//edit assignment
	@RequestMapping(value = "admin/assignment/edit/{assignmentId}", method = RequestMethod.GET)
	public String editView(Model model, @PathVariable("assignmentId") int assignmentId){
		model.addAttribute("assignment", this.assignmentService.getAssignment(assignmentId));
		model.addAttribute("ListCourses", this.assignmentService.listCourses());
		return "admin/assignment/edit";
		
	}
	@RequestMapping(value = "admin/assignment/edit", method = RequestMethod.POST)
	public String processEdit(@ModelAttribute("assignment") Assignment assignment) {
		assignmentService.updateAssignment(assignment);;
		return "redirect:/admin/assignment/list";
	}
	//delete assgn
	@RequestMapping("admin/assignment/delete/{assignmentId}")
	public String delete(@PathVariable int assignmentId) {
		assignmentService.deleteAssignment(assignmentId); 
		return "redirect:/admin/assignment/list";
	}
	
	// USER ROLE ACTIVITIES HERE
	//----------------
	// USER ROLE --> add assignment get
			@RequestMapping(value = "user/assignment/add", method = RequestMethod.GET)
			public String addAssignmentByUserView(Model model, Principal principal) {
				User loggedUser = this.userService.findUserByUsername(principal
						.getName());
				Assignment assignment = new Assignment();
				model.addAttribute("assignment", assignment);
	
				model.addAttribute("ListCourses", this.courseService.filterByDepartment(loggedUser.getDepartment().getDeptId()));
				return "user/assignment/add";
			}

	// Add assignment post
			@RequestMapping(value = "user/assignment/add", method = RequestMethod.POST)
			public String processAssignmentByUser(RedirectAttributes redirectAttributes, @ModelAttribute("assignment") Assignment assignment) {
				try{
					this.assignmentService.saveAssignment(assignment);
					redirectAttributes.addFlashAttribute("message", "Assignment added successfully!");
					return "redirect:/user/assignment/add";
				} catch(Exception e){
					redirectAttributes.addFlashAttribute("message", "Oops! An error has occoured.");
					return "redirect:/user/assignment/add";
				}
				
				
			}

}
