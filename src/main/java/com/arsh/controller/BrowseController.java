package com.arsh.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;




import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.arsh.model.BrowseAssignment;
import com.arsh.model.Course;
import com.arsh.model.User;
import com.arsh.service.AssignmentService;
import com.arsh.service.CommentService;
import com.arsh.service.CourseService;
import com.arsh.service.DepartmentService;
import com.arsh.service.QuestionService;
import com.arsh.service.UserService;


@Controller
public class BrowseController {

	@Autowired
	AssignmentService assignmentService;

	@Autowired
	DepartmentService departmentService;

	@Autowired
	QuestionService questionService;

	@Autowired
	CommentService commentService;

	@Autowired
	UserService userService;

	@Autowired
	CourseService courseService;

	// landing page
	@RequestMapping(value = { "/", "/index" }, method = RequestMethod.GET)
	public String index(Model model, Principal principal) {
		// checking if user is logged in
		if (principal != null) {
			User loggedUser = this.userService.findUserByUsername(principal
					.getName());
			model.addAttribute("loggedUser", loggedUser);
		}
		// populate statistics
		model.addAttribute("userCount", this.userService.getUserCount());
		model.addAttribute("deptCount",
				this.departmentService.getDepartmentCount());
		model.addAttribute("courseCount", this.courseService.getCourseCount());
		model.addAttribute("assignmentCount",
				this.assignmentService.getAssignmentCount());
		model.addAttribute("questionCount",
				this.questionService.getQuestionCount());
		model.addAttribute("commentCount",
				this.commentService.getCommentCount());
		return "index";
	}

	// browse assignments
	@RequestMapping(value = "/browse", method = RequestMethod.GET)
	public String browseAssignments(Model model, Principal principal) {
		if (principal != null) {
			User loggedUser = this.userService.findUserByUsername(principal
					.getName());
			model.addAttribute("loggedUser", loggedUser);
		}
		BrowseAssignment browseAssignment = new BrowseAssignment();
		model.addAttribute("listCourses", this.assignmentService.listCourses());
		model.addAttribute("listDepartments",
				this.departmentService.listDepartments());
		model.addAttribute("browseAssignment", browseAssignment);
		
		return "browse";
	}

	// load assignments
	@RequestMapping(value = "/browse", method = RequestMethod.POST)
	public String loadAssignments(
			Model model,
			RedirectAttributes redirectAttributes,
			@ModelAttribute("browseAssignment") BrowseAssignment browseAssignment) {

		redirectAttributes.addFlashAttribute("listAssignments",
				this.assignmentService.filterByCourse(browseAssignment
						.getCourseId()));
		return "redirect:/browse";
	}

	// view assignment
	@RequestMapping(value = "browse/assignment/{assignmentId}", method = RequestMethod.GET)
	public String viewAssignment(Model model, Principal principal,
			@PathVariable("assignmentId") int assignmentId) {
		// checking if user is logged in
		if (principal != null) {
			User loggedUser = this.userService.findUserByUsername(principal
					.getName());
			model.addAttribute("loggedUser", loggedUser);
		}
		
		model.addAttribute("assignment",
				this.assignmentService.getAssignment(assignmentId));
		model.addAttribute("listQuestions",
				this.questionService.listQuestionsByAssignmentId(assignmentId));
		return "browse/assignment";

	}
	
	@RequestMapping(value = "test", method = RequestMethod.GET)
	public String testVm(Model model, Principal principal){
		if (principal != null) {
			User loggedUser = this.userService.findUserByUsername(principal
					.getName());
			model.addAttribute("loggedUser", loggedUser);
		}
		return "velocity/test";
	}

	// view comments for questions on assignments
	@RequestMapping(value = "browse/comments", method = RequestMethod.GET)
	public String viewComment(Principal principal, Model model, 
			@RequestParam("assignmentId") int assignmentId,
			@RequestParam("questionId") int questionId) {
		// checking if user is logged in
				if (principal != null) {
					User loggedUser = this.userService.findUserByUsername(principal
							.getName());
					model.addAttribute("loggedUser", loggedUser);
				}
		model.addAttribute("question",
				this.questionService.getQuestion(questionId));
		model.addAttribute("assignment",
				this.assignmentService.getAssignment(assignmentId));
		model.addAttribute("listComments",
				this.commentService.listCommentsByQuestion(questionId));
		return "browse/comments";
	}
	
	//fetch courses for departments for ajax call
	@SuppressWarnings("unchecked")
	@RequestMapping(produces="application/json", value = "/fetchCoursesForDepartment", method = RequestMethod.GET)
	public @ResponseBody List<Course> coursesForDepartment(
			@RequestParam(value = "deptId") int deptId) {
			List<Course> courses = this.courseService.filterByDepartment(deptId);
			List<Course> filterCourses = new ArrayList<Course>();
			for(Course c: courses){
				Course c1 = new Course();
				String name = c.getName();
				int id = c.getCourseId();
				c1.setCourseId(id);
				c1.setName(name);
				filterCourses.add(c1);
			}
			return filterCourses;
	}
	
	

}
