package com.arsh.controller;

import java.security.Principal;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import com.arsh.model.Assignment;
import com.arsh.model.Comment;
 
import com.arsh.model.Course;
import com.arsh.model.User;
 
import com.arsh.service.CommentService;
import com.arsh.service.QuestionService;
import com.arsh.service.UserService;

@Controller
public class CommentController {

	@Autowired
	CommentService commentService;

	@Autowired
	private QuestionService questionService;

	@Autowired
	private UserService userService;

	// list comments
	@RequestMapping(value = "admin/comment/list", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("listComments", this.commentService.listComments());
		return "admin/comment/list";
	}

	// add comment- passed question ID
	@RequestMapping(value = "admin/comment/add/{questionId}", method = RequestMethod.GET)
	public String addView(Model model,
			@PathVariable("questionId") int questionId, Principal principal) {
		Comment comment = new Comment();
		// set assignment
		comment.setQuestion(this.questionService.getQuestion(questionId));
		// get logged user
		User loggedUser = this.userService.findUserByUsername(principal
				.getName());
		// send user id
		model.addAttribute("loggedUserId", loggedUser.getUserId());
		model.addAttribute("comment", comment);
		return "admin/comment/add";
	}
	
	//delete comment
	@RequestMapping("admin/comment/delete/{commentId}")
	public String deleteComment(@PathVariable int commentId) {
		commentService.deleteComment(commentId);
		return "redirect:/admin/comment/list";
	}

	// add question post
	@RequestMapping(value = "admin/comment/add", method = RequestMethod.POST)
	public String processAdd(@ModelAttribute("comment") Comment comment,
			@RequestParam("questionId") int questionId,
			@RequestParam("userId") long userId) {
		comment.setUser(userService.getUser(userId));
		comment.setQuestion(questionService.getQuestion(questionId));
		this.commentService.saveComment(comment);
		return "redirect:/admin/comment/list";
	}
	
	//USER ROLE ACTIVITY HERE------
	// add comment- passed question ID
		@RequestMapping(value = "user/comment/add/{questionId}", method = RequestMethod.GET)
		public String addUserCommentView(Model model,
				@PathVariable("questionId") int questionId, Principal principal) {
			Comment comment = new Comment();
			// set assignment
			comment.setQuestion(this.questionService.getQuestion(questionId));
			// get logged user
			User loggedUser = this.userService.findUserByUsername(principal
					.getName());
			// send user for users id
			model.addAttribute("loggedUser", loggedUser);
			model.addAttribute("comment", comment);
			return "user/comment/add";
		}
		// add question post
		@RequestMapping(value = "user/comment/add", method = RequestMethod.POST)
		public String processUserAddComment(@ModelAttribute("comment") Comment comment,
				@RequestParam("questionId") int questionId,@RequestParam("assignmentId") int assignmentId,
				@RequestParam("userId") long userId) {
			comment.setUser(userService.getUser(userId));
			comment.setQuestion(questionService.getQuestion(questionId));
			this.commentService.saveComment(comment);
			return "redirect:/browse/comments?assignmentId="+assignmentId+"&questionId="+questionId;
		}
		
		//fetch courses for departments for ajax call
		@SuppressWarnings("unchecked")
		@RequestMapping(produces="application/json", value = "/comment/report-spam/{commentId}", method = RequestMethod.POST)
		public void spamReport(@PathVariable("commentId") int commentId){
			System.out.println(commentId);
			System.out.println("received from ajaxs");
			 

		}
		
		
	

}
