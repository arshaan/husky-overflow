package com.arsh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.arsh.model.Course;
import com.arsh.model.Department;
import com.arsh.model.User;
import com.arsh.service.CourseService;

@Controller
public class CourseController {
	
	@Autowired
	private CourseService courseService;
	
	// add new course - GET
	// this request has come from department view page. Hence deptId present
		/*@RequestMapping(value = "admin/course/add/{deptId}", method = RequestMethod.GET)
		public String addCourseView(@PathVariable("deptId") int deptId, Model model) {
			Course course = new Course();
			course.setDepartment(this.courseService.getDepartment(deptId));
		
			model.addAttribute("course", course);
			model.addAttribute("deptID", this.courseService.getDepartment(deptId).getDeptId());
			return "admin/course/add";
		}*/
	//Post	
		/*	@RequestMapping(value = "admin/course/add", method = RequestMethod.POST)
			public String processAdd(@RequestParam("DEPT_ID") int deptId, @ModelAttribute("course") Course course) {
				course.setDepartment(this.courseService.getDepartment(deptId));
			
				this.courseService.saveCourse(course);
				return "redirect:/admin/course/list";
			}*/
		//add course
		@RequestMapping(value = "admin/course/add", method = RequestMethod.GET)
		public String addCourseView( Model model) {
			Course course = new Course();
			model.addAttribute("course", course);
			model.addAttribute("departments", this.courseService.listDepartments());
			return "admin/course/add";
		}
	//Post	

		@RequestMapping(value = "admin/course/add", method = RequestMethod.POST)
		public String processAdd( @ModelAttribute("course") Course course) {
			this.courseService.saveCourse(course);
			return "redirect:/admin/course/list";
		}
	// list all courses
		@RequestMapping(value = "admin/course/list", method = RequestMethod.GET)
		public String list(Model model) {
			model.addAttribute("course", new Course());
			model.addAttribute("listCourses", this.courseService.listCourses());
			return "admin/course/list";
		}
	//edit course
		@RequestMapping(value = "admin/course/edit/{courseId}", method = RequestMethod.GET)
		public String editView(Model model, @PathVariable("courseId") int courseId){
			model.addAttribute("course", this.courseService.getCourse(courseId));
			model.addAttribute("departments", this.courseService.listDepartments());
			return "admin/course/edit";
			
		}
		@RequestMapping(value = "admin/course/edit", method = RequestMethod.POST)
		public String processEditCourse(@ModelAttribute("course") Course course) {
			courseService.updateCourse(course);
			return "redirect:/admin/course/list";
		}
		//delete course
		@RequestMapping("admin/course/delete/{courseId}")
		public String deleteUser(@PathVariable int courseId) {
			courseService.deleteCourse(courseId); 
			return "redirect:/admin/course/list";
		}

}
