package com.arsh.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.arsh.dao.DepartmentDao;
import com.arsh.model.Course;
import com.arsh.model.Department;
import com.arsh.service.DepartmentService;

@Controller
public class DepartmentController {

	@Autowired
	private DepartmentService departmentService;

	// add new department - GET
	@RequestMapping(value = "admin/department/add", method = RequestMethod.GET)
	public String addDeptView(Model model) {
		model.addAttribute("department", new Department());
		return "admin/department/add";
	}

	// Post
	@RequestMapping(value = "admin/department/add", method = RequestMethod.POST)
	public String processAdd(@ModelAttribute("department") Department department) {
		this.departmentService.saveDepartment(department);
		return "redirect:/admin/department/list";
	}

	// list all depts
	@RequestMapping(value = "admin/department/list", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("department", new Department());
		model.addAttribute("listDepartments",
				this.departmentService.listDepartments());
		return "admin/department/list";
	}

	// edit
	@RequestMapping(value = "admin/department/edit/{deptId}", method = RequestMethod.GET)
	public String editView(Model model, @PathVariable("deptId") int deptId) {
		model.addAttribute("department",
				this.departmentService.getDepartment(deptId));
		return "admin/department/edit";

	}

	@RequestMapping(value = "admin/department/edit", method = RequestMethod.POST)
	public String processEditDepartment(
			@ModelAttribute("department") Department department) {
		departmentService.updateDepartment(department);
		return "redirect:/admin/department/list";
	}
	//delete department
			@RequestMapping("admin/department/delete/{deptId}")
			public String deleteUser(@PathVariable int deptId) {
				departmentService.deleteDepartment(deptId);
				return "redirect:/admin/department/list";
			}

}
