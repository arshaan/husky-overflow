package com.arsh.controller;

import java.security.Principal;

 

import org.springframework.beans.factory.annotation.Autowired;
 
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;




import org.springframework.web.bind.annotation.RequestParam;

import com.arsh.model.Question;
import com.arsh.model.User;
import com.arsh.service.AssignmentService;
import com.arsh.service.QuestionService;
import com.arsh.service.UserService;

@Controller
public class QuestionController {

	@Autowired
	private QuestionService questionService;

	@Autowired
	private AssignmentService assignmentService;
	
	@Autowired
	private UserService userService;
	
	
	// list questions
	@RequestMapping(value = "admin/question/list", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("question", new Question());
		model.addAttribute("listQuestions",
				this.questionService.listQuestions());
		return "admin/question/list";
	}

	// add question get - passed assignment ID
	@RequestMapping(value = "admin/question/add/{assignmentId}", method = RequestMethod.GET)
	public String addView(Model model, @PathVariable("assignmentId") int assignmentId, Principal principal) {
		Question question = new Question();
		//set assignment
		question.setAssignment(this.assignmentService.getAssignment(assignmentId));
		//get logged user
		User loggedUser = this.userService.findUserByUsername(principal.getName());
	    //send user id
		model.addAttribute("loggedUserId", loggedUser.getUserId());
		model.addAttribute("question", question);
		return "admin/question/add";
	}

	// add question post
	@RequestMapping(value = "admin/question/add", method = RequestMethod.POST)
	public String processAdd(@ModelAttribute("question") Question question,
							@RequestParam("assignmentId") int assignmentId,
							@RequestParam("userId") long userId) {
		
		question.setUser(userService.getUser(userId));
		question.setAssignment(assignmentService.getAssignment(assignmentId));
		this.questionService.saveQuestion(question);
		
		return "redirect:/admin/question/list";
	}
	//edit assignment
		@RequestMapping(value = "admin/question/edit/{questionId}", method = RequestMethod.GET)
		public String editView(Model model, @PathVariable("questionId") int questionId){
			model.addAttribute("question", this.questionService.getQuestion(questionId));
			model.addAttribute("listAssignments", this.questionService.listAssignments());
			return "admin/question/edit";
			
		}
		@RequestMapping(value = "admin/question/edit", method = RequestMethod.POST)
		public String processEdit( @ModelAttribute("question") Question question) {
			questionService.updateQuestion(question);;
			return "redirect:/admin/question/list";
		}
		//delete question
		@RequestMapping("admin/question/delete/{questionId}")
		public String deleteQuestion(@PathVariable int questionId) {
			questionService.deleteQuestion(questionId); 
			return "redirect:/admin/question/list";
		}
		
		//USER ROLE ACTIVITY HERE
		// add question get - passed assignment ID
		@RequestMapping(value = "user/question/add/{assignmentId}", method = RequestMethod.GET)
		public String addUserQuestionView(Model model, @PathVariable("assignmentId") int assignmentId, Principal principal) {
			Question question = new Question();
			//set assignment
			question.setAssignment(this.assignmentService.getAssignment(assignmentId));
			//get logged user
			User loggedUser = this.userService.findUserByUsername(principal.getName());
		    //send user id
			model.addAttribute("loggedUser", loggedUser);
			model.addAttribute("question", question);
			return "user/question/add";
		}
		
		// add question post
		@RequestMapping(value = "user/question/add", method = RequestMethod.POST)
		public String processUserQuestionAdd(@ModelAttribute("question") Question question,
								@RequestParam("assignmentId") int assignmentId,
								@RequestParam("userId") long userId) {
			question.setUser(userService.getUser(userId));
			question.setAssignment(assignmentService.getAssignment(assignmentId));
			this.questionService.saveQuestion(question);
			return "redirect:/browse/assignment/"+assignmentId;
		}
}
