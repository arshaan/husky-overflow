package com.arsh.controller;

import java.io.BufferedOutputStream;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.security.Principal;
import java.util.Date;
import java.util.Set;
import java.util.UUID;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.springframework.security.core.authority.AuthorityUtils;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.Validator;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.arsh.model.User;
import com.arsh.service.UserService;
import com.arsh.serviceImpl.ApplicationMailer;

@Controller
public class UserController {

	@Autowired
	private UserService userService;
	@Autowired
	private ServletContext servletContext;

	ApplicationContext context = new ClassPathXmlApplicationContext(
			"emailer.xml");

	ApplicationMailer mailer = (ApplicationMailer) context
			.getBean("applicationMailer");

	/*
	 * Specify this useValidate will be injected
	 */
	@Autowired
	@Qualifier("userValidator")
	private Validator validator;

	/*
	 * This is to initialize webDataBinder,set its validator as we specify.
	 */

	@InitBinder
	private void initBinder(WebDataBinder binder) {
		binder.setValidator(validator);
	}

	@RequestMapping(value = "/home", method = RequestMethod.GET)
	protected String welcome(Model model, Principal principal,
			RedirectAttributes redirectAttributes) {

		Set<String> roles = AuthorityUtils
				.authorityListToSet(SecurityContextHolder.getContext()
						.getAuthentication().getAuthorities());

		User loggedUser = this.userService.findUserByUsername(principal
				.getName());

		if (roles.contains("ROLE_ADMIN")) {
			redirectAttributes.addFlashAttribute("loggedUser", loggedUser);
			return "admin/admin-welcome";
		} else if (roles.contains("ROLE_USER")) {
			redirectAttributes.addFlashAttribute("loggedUser", loggedUser);
			return "redirect:/index";
		} else {
			return "redirect:/index";
		}
	}

	// list all users
	@RequestMapping(value = "admin/user/list", method = RequestMethod.GET)
	public String list(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("listUsers", this.userService.listUsers());
		return "admin/user/list";
	}

	// add new user - GET
	@RequestMapping(value = "admin/user/add", method = RequestMethod.GET)
	public String addUserView(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("departments", this.userService.listDepartments());
		return "admin/user/add";
	}

	// process add user
	// @RequestMapping(value = {"admin/add-user", "register-user"}, method =
	// RequestMethod.POST)
	@RequestMapping(value = "admin/user/add", method = RequestMethod.POST)
	public String processAdd(@ModelAttribute("user") User user) {
		this.userService.saveUser(user);
		return "redirect:/admin/user/list";
	}

	// delete
	@RequestMapping("admin/user/delete/{userId}")
	public String deleteUser(@PathVariable long userId) {
		userService.deleteUser(userId);
		return "redirect:/admin/user/list";
	}

	// edit
	@RequestMapping(value = "admin/user/edit/{userId}", method = RequestMethod.GET)
	public String editUserView(@PathVariable("userId") Long id, Model model) {
		model.addAttribute("user", this.userService.getUser(id));
		model.addAttribute("departments", this.userService.listDepartments());
		return "/admin/user/edit";
	}

	@RequestMapping(value = "admin/user/edit", method = RequestMethod.POST)
	public String processEditUser(@ModelAttribute("user") User user) {
		userService.updateUser(user);
		return "redirect:/admin/user/list";
	}

	// login
	@RequestMapping(value = "/login", method = RequestMethod.GET)
	public String login(ModelMap model,
			@RequestParam(value = "error", required = false) boolean error) {
		if (error == true) {
			// Assign an error message
			model.addAttribute("error", "true");
		}
		return "login";
	}

	// logout
	@RequestMapping(value = "/logout", method = RequestMethod.GET)
	public String logout(ModelMap model) {
		return "logout";
	}

	@RequestMapping(value = "/accessdenied", method = RequestMethod.GET)
	public String loginerror(ModelMap model) {
		model.addAttribute("error", "true");
		return "denied";
	}

	// add new user - GET
	@RequestMapping(value = "/register", method = RequestMethod.GET)
	public String registerUserView(Model model) {
		model.addAttribute("user", new User());
		model.addAttribute("departments", this.userService.listDepartments());
		return "register";
	}

	// user registration processing
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public String processRegisterUser(
			@ModelAttribute("user") @Validated User user, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			System.out.println("Error has occoured while user registration");

			model.addAttribute("departments",
					this.userService.listDepartments());
			return "register";
		} else {
			try {
				// create new user
				user.setRole("ROLE_USER");
				// generate random token and persist
				String token = UUID.randomUUID().toString();
				user.setToken(token);
				String uncodedPwd = user.getPassword();
				// encode password before saving
				BCryptPasswordEncoder passwordEncoder = new BCryptPasswordEncoder();
				String hashedPassword = passwordEncoder.encode(user
						.getPassword());
				user.setPassword(hashedPassword);
				this.userService.saveUser(user);
				// send email here
				try {
					mailer.sendMail(
							"no-reply@huskyoverflow.com",
							user.getUsername(),
							"Welcome to Husky Overflow - Activate Your Account",
							"Hi"
									+ " "
									+ user.getName()
									+ ","
									+ "\n"
									+ "Please activate your account by clicking on the link below\n"
									+ "http://localhost:3030" //change this later
									+ servletContext.getContextPath()
									+ "/validateAccount?username="
									+ user.getUsername() + "&token="
									+ user.getToken() + "\n" + "\n"
									+ "Your Account Details" + "\n"
									+ "Username: " + user.getUsername() + "\n"
									+ "Password: " + uncodedPwd);

					model.addAttribute("regmailSent", "true");
				} catch (Exception e) {
					System.out.println(e.getMessage());
				}
				return "register";
			} catch (Exception e) {
				model.addAttribute("alert", "Email already exists");
				model.addAttribute("departments",
						this.userService.listDepartments());
				return "register";
			}

		}
	}

	// email activation
	@RequestMapping(value = "/validateAccount", method = RequestMethod.GET)
	public String validateUser(Model model,
			@RequestParam("token") String receivedToken,
			@RequestParam("username") String username,
			@ModelAttribute("user") User user) {

		user = this.userService.findUserByUsername(username);

		try {

			if (user != null) {
				if (user.getToken().equals(receivedToken)) {
					user.setEnabled(true);
					this.userService.updateUser(user);
					model.addAttribute("validationSuccess", "true");
					// send email with account details
					try {
						mailer.sendMail(
								"no-reply@huskyoverflow.com",
								user.getUsername(),
								"Welcome to Husky Overflow - Account Activated",
								"Hi"
										+ " "
										+ user.getName()
										+ ","
										+ "\n"
										+ "Welcome to the community. Your account has been activated successfully!"
										+ "\n");
					} catch (Exception e) {
						System.out.println(e.getMessage());
					}
				} else {
					model.addAttribute("alert",
							"Oops! Email could not be validated.");
				}
			}
		} catch (Exception e) {
			System.out.println(e.getMessage());
			e.printStackTrace();
		}
		return "login";
	}

	// USER ROLE--
	// Profile edit - get
	@RequestMapping(value = "user/profile/edit", method = RequestMethod.GET)
	public String profileEdit(Model model, Principal principal) {
		User loggedUser = this.userService.findUserByUsername(principal
				.getName());
		model.addAttribute("loggedUser", loggedUser);
		model.addAttribute("departments", this.userService.listDepartments());
		return "user/profile/edit";
	}

	// profile edit post
	@RequestMapping(value = "user/profile/edit", method = RequestMethod.POST)
	public String processEditProfile(
			@ModelAttribute("user") @Validated User user, BindingResult result,
			Model model) {
		if (result.hasErrors()) {
			System.out.println("Error has occoured while user registration");
			return "redirect:/user/profile/edit";
		} try{
			user.setRole("ROLE_USER");
			user.setEnabled(true);
			userService.updateUser(user);
			return "redirect:/user/profile/view";
		} catch(Exception e ){
			System.out.println("Error has occoured while user registration -- >>> "+ e.getMessage());
			
		}
		  return "redirect:/user/profile/edit";
	}

	// profile view
	@RequestMapping(value = "user/profile/view", method = RequestMethod.GET)
	public String profileView(Model model, Principal principal) {
		User loggedUser = this.userService.findUserByUsername(principal
				.getName());
		model.addAttribute("loggedUser", loggedUser);
		return "user/profile/view";
	}

	@RequestMapping(value = "/profile/image/{userId}", method = RequestMethod.GET)
	public void getuserImage(@PathVariable long userId, HttpSession session,
			HttpServletResponse response) {
		OutputStream oImage;

		try {
			User u = this.userService.getUser(userId);
			byte[] imageInByte = u.getAvtar();
			response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
			oImage = response.getOutputStream();
			oImage.write(imageInByte);
			oImage.flush();
			oImage.close();
		} catch (Exception e) {
			System.out.println("image error");
			e.printStackTrace();
		}
	}

	/**
	 * Enable image upload
	 */
	@RequestMapping(value = "/user/profile/uploadProfilePic", method = RequestMethod.POST)
	public String uploadFileHandler(RedirectAttributes redirectAttributes,
			Principal principal, @RequestParam("file") MultipartFile file,
			HttpServletRequest request, HttpSession session) {

		if (!file.isEmpty()) {
			System.out.println(file.getContentType());
			ServletContext context = session.getServletContext();
			String realContextPath = context.getRealPath(request
					.getContextPath());

			System.out.println("context path is " + realContextPath);
			try {
				validateImage(file);
			} catch (Exception e) {
				System.out.println("Only image accepted");
				redirectAttributes.addFlashAttribute("error", e.getMessage());
				return "redirect:/user/profile/edit";
			}
			try {

				byte[] bytes = file.getBytes();
				String imageName = file.getOriginalFilename();
				String ext = imageName.substring(imageName.length() - 4);

				User loggedUser = this.userService.findUserByUsername(principal
						.getName());
				// Creating the directory to store file
				// String rootPath = System.getProperty("catalina.home");

				File dir = new File(realContextPath + "/profile-pictures/"
						+ loggedUser.getName() + imageName);
				if (!dir.exists())
					dir.mkdirs();

				// Create the file on server
				File serverFile = new File(dir + File.separator
						+ loggedUser.getName() + "_" + loggedUser.getUserId() +

						ext);
				BufferedOutputStream stream = new BufferedOutputStream(
						new FileOutputStream(serverFile));
				stream.write(bytes);
				stream.close();
				// trying to save in db
				FileInputStream fileInputStream = new FileInputStream(
						serverFile);
				// convert file into array of bytes
				fileInputStream.read(bytes);
				fileInputStream.close();
				loggedUser.setAvtar(bytes);
				this.userService.updateUser(loggedUser);
				return "redirect:/user/profile/view";
			} catch (Exception e) {

				System.out.println("You failed to upload " + e.getMessage());
				redirectAttributes.addFlashAttribute("error",
						"You failed to upload " + e.getMessage());
				return "redirect:/user/profile/edit";

			}
		} else {
			System.out
					.println("You failed to upload because the file was empty.");
			redirectAttributes.addFlashAttribute("error",
					"You failed to upload because the file was empty.");
			return "redirect:/user/profile/edit";
		}
	}

	private void validateImage(MultipartFile file) {
		if (!file.getContentType().equals("image/jpeg")) {
			throw new RuntimeException("Only images are accepted");
		}

	}

}
