package com.arsh.dao;

import java.util.List;

import com.arsh.model.Assignment;
import com.arsh.model.Course;




public interface AssignmentDao {
	
	/*
     * CREATE and UPDATE
     */
    public void saveAssignment(Assignment assignment); 
   
    public void updateAssignment(Assignment assignment);
    /*
     * READ
     */
    public List<Assignment> listAssignment();
    
    public List<Assignment> filterByCourse(int courseId);
    
    public Assignment getAssignment(int id);
  
    public List<Course> listCourses();
    
    public long getAssignmentCount();

    /*
     * DELETE
     */
    public void deleteAssignment(int id);

}
