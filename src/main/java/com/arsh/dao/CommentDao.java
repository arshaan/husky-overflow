package com.arsh.dao;

import java.util.List;

 
import com.arsh.model.Comment;
 

public interface CommentDao {
	/*
     * CREATE and UPDATE
     */
    public void saveComment(Comment comment); 
   
    public void updateComment(Comment comment);
    /*
     * READ
     */
    public List<Comment> listComments();
    
    public List<Comment> listCommentsByQuestion(int questionId);
    
    public Comment getComment(int id);
  
    public long getCommentCount();

    /*
     * DELETE
     */
    public void deleteComment(int id);
}
