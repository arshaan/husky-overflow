package com.arsh.dao;



import java.util.List;

import com.arsh.model.Assignment;
import com.arsh.model.Course;
import com.arsh.model.Department;



public interface CourseDao {
	
	/*
     * CREATE and UPDATE
     */
    public void saveCourse(Course course); 
   
    public void updateCourse(Course course);
    /*
     * READ
     */
    public List<Course> listCourses();
    
    public Course getCourse(int id);
    
    public Department getDepartment(int id);
    
    public List<Department> listDepartments();
    
    public List<Course> filterByDepartment(int deptId);
    
    public long getCourseCount();

    /*
     * DELETE
     */
    public void deleteCourse(int id);

}
