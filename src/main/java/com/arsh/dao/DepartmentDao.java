package com.arsh.dao;

import java.util.List;

import com.arsh.model.Department;



public interface DepartmentDao {
	/*
     * CREATE and UPDATE
     */
    public void saveDepartment(Department department); 
    
    public void updateDepartment(Department department);
    /*
     * READ
     */
    public List<Department> listDepartments();
    
    public Department getDepartment(int id);
    
    public long getDepartmentCount();

    /*
     * DELETE
     */
    public void deleteDepartment(int id);
}
