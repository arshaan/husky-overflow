package com.arsh.dao;



import java.util.List;

import com.arsh.model.Department;
import com.arsh.model.User;


public interface UserDao {
	
	/*
     * CREATE and UPDATE
     */
    public void saveUser(User user); 
    
    public void updateUser(User user);
    
    /*
     * READ
     */
    public List<User> listUsers();
    
    public User getUser(Long id);
    
    public List<Department> listDepartments();
    
    public User findUserByUsername(String username);
    
    public long getUserCount();
    /*
     * DELETE
     */
    public void deleteUser(Long id);

}
