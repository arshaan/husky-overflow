package com.arsh.daoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arsh.dao.AssignmentDao;
import com.arsh.model.Assignment;
import com.arsh.model.Course;
import com.arsh.model.User;


//Indicates that the annotated class is a Repository or DAO
@Repository
public class AssignmentDaoImpl implements AssignmentDao{
	
	// The field is injected
		@Autowired
		private SessionFactory sessionFactory;

		public Session getSession() {
			// singleton
			Session sess = getSessionFactory().getCurrentSession();
			if (sess == null) {
				sess = getSessionFactory().openSession();
			}
			return sess;
		}

		private SessionFactory getSessionFactory() {
			return sessionFactory;
		}


	@Override
	public void saveAssignment(Assignment assignment) {
		getSession().save(assignment);
		
	}

	@Override
	public void updateAssignment(Assignment assignment) {
		getSession().update(assignment);
		
	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Assignment> listAssignment() {
		return getSession().createCriteria(Assignment.class).list();
	}

	@Override
	public Assignment getAssignment(int id) {
		return (Assignment) getSession().get(Assignment.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> listCourses() {
		return getSession().createCriteria(Course.class).list();
	}

	@Override
	public void deleteAssignment(int id) {
		Assignment assignment = getAssignment(id);
		 if (null != assignment) {
            getSession().delete(assignment);
     }
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Assignment> filterByCourse(int courseId) {
		Criteria criteria = getSession().createCriteria(Assignment.class);
		criteria.add(Restrictions.eq("course.courseId", courseId));
		return  criteria.list();
	}

	@Override
	public long getAssignmentCount() {
		
		return (Long) getSession().createQuery("select count(*) from Assignment").uniqueResult();

	}

}
