package com.arsh.daoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arsh.dao.CommentDao;
import com.arsh.model.Comment;
import com.arsh.model.Question;
 
@Repository
public class CommentDaoImpl implements CommentDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		// singleton
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Override
	public void saveComment(Comment comment) {
		getSession().save(comment);
		
	}

	@Override
	public void updateComment(Comment comment) {
		getSession().update(comment);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> listComments() {
		 return getSession().createCriteria(Comment.class).list();
	}

	@Override
	public Comment getComment(int id) {
		return (Comment) getSession().get(Comment.class, id);
	}

	@Override
	public void deleteComment(int id) {
		Comment comment = getComment(id);
		 if (null != comment) {
            getSession().delete(comment);
     }
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Comment> listCommentsByQuestion(int questionId) {
		Criteria criteria = getSession().createCriteria(Comment.class);
		criteria.add(Restrictions.eq("question.questionId", questionId));
		return  criteria.list();
	}

	@Override
	public long getCommentCount() {
		return (Long) getSession().createQuery("select count(*) from Comment").uniqueResult();
		
	}

}
