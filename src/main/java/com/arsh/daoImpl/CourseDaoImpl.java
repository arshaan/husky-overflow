package com.arsh.daoImpl;

import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arsh.dao.CourseDao;
import com.arsh.model.Assignment;
import com.arsh.model.Course;
import com.arsh.model.Department;


//Indicates that the annotated class is a Repository or DAO
@Repository
public class CourseDaoImpl implements CourseDao {
	// The field is injected
	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		// singleton
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Override
	public void saveCourse(Course course) {
		getSession().save(course);

	}

	@Override
	public void updateCourse(Course course) {
		getSession().update(course);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Course> listCourses() {
		return getSession().createCriteria(Course.class).list();
	}

	@Override
	public Course getCourse(int id) {
		return (Course) getSession().get(Course.class, id);
	}

	@Override
	public void deleteCourse(int id) {
		Course course = getCourse(id);
		 if (null != course) {
             getSession().delete(course);
      }

	}

	@Override
	public Department getDepartment(int id) {
		
		 return (Department) getSession().get(Department.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Department> listDepartments() {
		return getSession().createCriteria(Department.class).list();
	}

	@Override
	public long getCourseCount() {
		return (Long) getSession().createQuery("select count(*) from Course").uniqueResult();

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Course> filterByDepartment(int deptId) {
		return getSession().createQuery("from Course where department.deptId ="+deptId).list();
		 
	}

}
