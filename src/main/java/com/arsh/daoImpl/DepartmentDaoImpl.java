package com.arsh.daoImpl;

import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arsh.dao.DepartmentDao;
import com.arsh.model.Department;
@Repository
public class DepartmentDaoImpl implements DepartmentDao{
	
	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		// singleton
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Override
	public void saveDepartment(Department department) {
		getSession().save(department);
		
	}

	@Override
	public void updateDepartment(Department department) {
		getSession().update(department);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Department> listDepartments() {
		return getSession().createCriteria(Department.class).list();
	}

	@Override
	public Department getDepartment(int id) {
		return (Department) getSession().get(Department.class, id);
	}

	@Override
	public void deleteDepartment(int id) {
		Department department = getDepartment(id);
		 if (null != department) {
            getSession().delete(department);
     }
		
	}

	@Override
	public long getDepartmentCount() {
		return (Long) getSession().createQuery("select count(*) from Department").uniqueResult();

	}

}
