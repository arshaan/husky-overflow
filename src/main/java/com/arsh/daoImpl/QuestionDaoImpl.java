package com.arsh.daoImpl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arsh.dao.QuestionDao;
import com.arsh.model.Assignment;
import com.arsh.model.Question;

@Repository
public class QuestionDaoImpl implements QuestionDao {
	// The field is injected
	@Autowired
	private SessionFactory sessionFactory;

	public Session getSession() {
		// singleton
		Session sess = getSessionFactory().getCurrentSession();
		if (sess == null) {
			sess = getSessionFactory().openSession();
		}
		return sess;
	}

	private SessionFactory getSessionFactory() {
		return sessionFactory;
	}

	@Override
	public void saveQuestion(Question question) {
		question.setCreated(new Date());
		getSession().save(question);

	}

	@Override
	public void updateQuestion(Question question) {
		getSession().update(question);

	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Question> listQuestions() {
		return getSession().createCriteria(Question.class).list();
	}

	@Override
	public Question getQuestion(int id) {
		return (Question) getSession().get(Question.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Assignment> listAssignments() {
		return getSession().createCriteria(Assignment.class).list();

	}

	@Override
	public void deleteQuestion(int id) {
		Question question = getQuestion(id);
		if (null != question) {
			getSession().delete(question);
		}

	}
	
	@SuppressWarnings("unchecked")
	@Override
	public List<Question> listQuestionsByAssignmentId(int assignmentId) {
		Criteria criteria = getSession().createCriteria(Question.class);
		criteria.add(Restrictions.eq("assignment.assignmentId", assignmentId));
		return  criteria.list();
	}

	@Override
	public long getQuestionCount() {
		return (Long) getSession().createQuery("select count(*) from Question").uniqueResult();

	}

}
