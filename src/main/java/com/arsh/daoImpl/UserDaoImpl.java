package com.arsh.daoImpl;

import java.util.Date;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.arsh.dao.UserDao;
import com.arsh.model.Department;
import com.arsh.model.User;

//Indicates that the annotated class (UserDaoImpl) is a Repository or DAO
@Repository
public class UserDaoImpl implements UserDao{
	
//The field is injected 
	@Autowired
    private SessionFactory sessionFactory;
	
	public Session getSession() { 
		//singleton
         Session sess = getSessionFactory().getCurrentSession();
         if (sess == null) {
                sess = getSessionFactory().openSession();
         }
         return sess;
  }

  private SessionFactory getSessionFactory() {
         return sessionFactory;
  }

	@Override
	public void saveUser(User user) {
		//session merge takes care of both save and update
		user.setCreated(new Date());
		getSession().save(user);
		
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<User> listUsers() {
		 return getSession().createCriteria(User.class).list();
	}

	@Override
	public User getUser(Long id) {
		  return (User) getSession().get(User.class, id);
	}

	@Override
	public void deleteUser(Long id) {
		User user = getUser(id);

         if (null != user) {
                getSession().delete(user);
         }
		
	}

	@Override
	public void updateUser(User user) {
		getSession().update(user);
		
	}
	@SuppressWarnings("unchecked")
	@Override
	public List<Department> listDepartments() {
		return getSession().createCriteria(Department.class).list();
	}

	@Override
	public User findUserByUsername(String username) {
		 Criteria criteria = getSession().createCriteria(User.class);
		 criteria.add(Restrictions.eq("username", username));
		 return (User) criteria.uniqueResult();
	}

	@Override
	public long getUserCount() {
		return (Long) getSession().createQuery("select count(*) from User").uniqueResult();

	}

}
