package com.arsh.model;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToMany;
import javax.persistence.ManyToOne;
import javax.persistence.OneToMany;


@Entity

public class Assignment {
	@Id
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private int assignmentId;
	private String title;
	private String description;
	private Date created;
	
	//one assignments can have many questions
	@OneToMany
	private Collection <Question> questionList = new ArrayList<Question>();
	
	public Collection<Question> getQuestionList() {
		return questionList;
	}
	public void setQuestionList(Collection<Question> questionList) {
		this.questionList = questionList;
	}
	//many assignment  will be associated with one course
	@ManyToOne
	
	private Course course;
	
	
	public Course getCourse() {
		return course;
	}
	public void setCourse(Course course) {
		this.course = course;
	}
	public int getAssignmentId() {
		return assignmentId;
	}
	public void setAssignmentId(int assignmentId) {
		this.assignmentId = assignmentId;
	}
	public String getTitle() {
		return title;
	}
	public void setTitle(String title) {
		this.title = title;
	}
	public String getDescription() {
		return description;
	}
	public void setDescription(String description) {
		this.description = description;
	}
	public Date getCreated() {
		return created;
	}
	public void setCreated(Date created) {
		this.created = created;
	}
	
	
	

}
