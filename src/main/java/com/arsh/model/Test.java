package com.arsh.model;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.cfg.Configuration;


public class Test{
	
	public static void main(String args[]){
		//user
		User u = new User();
		u.setName("Admin");
		u.setUsername("admin@gmail.com");
		u.setPassword("a");
		u.setRole("ROLE_ADMIN");
		u.setEnabled(true);
		
		//dept
		Department dep = new Department();
		dep.setDepartment("Information Systems");
		u.setDepartment(dep);
		dep.setUser(u);
		
		//
		Course c1 = new Course();
		c1.setName("AED");
		dep.getCourseList().add(c1);
		c1.setDepartment(dep);
		
		Assignment a1 = new Assignment();
		a1.setTitle("AED Assignment-1");
		a1.setCourse(c1);
		c1.getAssignmentList().add(a1);
		 
		SessionFactory sessionFac = new Configuration().configure().buildSessionFactory();
		Session session = sessionFac.openSession();
		session.beginTransaction();
		session.save(u);
		session.save(c1);
	
		session.save(a1);
		
		
		session.save(dep);
		session.getTransaction().commit();
		session.close();
		System.out.println("Inserted successfully");
		
	}
}