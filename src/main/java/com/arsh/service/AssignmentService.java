package com.arsh.service;

import java.util.List;

import com.arsh.model.Assignment;
import com.arsh.model.Course;

public interface AssignmentService {
	/*
     * CREATE and UPDATE
     */
    public void saveAssignment(Assignment assignment); 
   
    public void updateAssignment(Assignment assignment);
    /*
     * READ
     */
    public List<Assignment> listAssignment();
    
    public Assignment getAssignment(int id);
  
    public List<Course> listCourses();
    
    public List<Assignment> filterByCourse(int courseId);
    
    public long getAssignmentCount();

    /*
     * DELETE
     */
    public void deleteAssignment(int id);
}
