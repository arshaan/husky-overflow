package com.arsh.service;

import java.util.List;

import com.arsh.model.Comment;

public interface CommentService {
	/*
     * CREATE and UPDATE
     */
    public void saveComment(Comment comment); 
   
    public void updateComment(Comment comment);
    /*
     * READ
     */
    public List<Comment> listComments();
    
    public Comment getComment(int id);
    
    public List<Comment> listCommentsByQuestion(int questionId);
    
    public long getCommentCount();
     

    /*
     * DELETE
     */
    public void deleteComment(int id);
}
