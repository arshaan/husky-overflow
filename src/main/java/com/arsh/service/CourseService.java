package com.arsh.service;

import java.util.List;

import com.arsh.model.Course;
import com.arsh.model.Department;

public interface CourseService {
	/*
     * CREATE and UPDATE
     */
    public void saveCourse(Course course); 
    public void updateCourse(Course course);

    /*
     * READ
     */
    public List<Course> listCourses();
    
    public Course getCourse(int id);
    
    public long getCourseCount();
    
    public List<Course> filterByDepartment(int deptId);
    //Departments
    public Department getDepartment(int id);
    
    public List<Department> listDepartments();
    
    /*
     * DELETE
     */
    public void deleteCourse(int id);
}
