package com.arsh.service;

import java.util.List;

import com.arsh.model.Assignment;
import com.arsh.model.Question;

public interface QuestionService {

	/*
	 * CREATE and UPDATE
	 */
	public void saveQuestion(Question question);

	public void updateQuestion(Question question);

	/*
	 * READ
	 */
	public List<Question> listQuestions();
	
	public List<Question> listQuestionsByAssignmentId(int assignmentId);

	public Question getQuestion(int id);

	public List<Assignment> listAssignments();
	
	public long getQuestionCount();

	/*
	 * DELETE
	 */
	public void deleteQuestion(int id);
}
