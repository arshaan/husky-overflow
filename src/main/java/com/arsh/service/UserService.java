package com.arsh.service;

import java.util.List;

import com.arsh.model.Department;
import com.arsh.model.User;

/*
 * The Service Layer is a bridge between the DAO (Persistence) layer and the Presentation (Web) layer. 
 */
public interface UserService {
	
	/*
     * CREATE 
     */
    public void saveUser(User user);
    
    public void updateUser(User user);

    /*
     * READ
     */
    public List<User> listUsers();
    
    public User getUser(Long id);
    
    public List<Department> listDepartments();
    
    public User findUserByUsername(String username);
    
    public long getUserCount();
    /*
     * DELETE
     */
    public void deleteUser(Long id);

}
