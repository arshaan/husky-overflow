package com.arsh.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arsh.dao.AssignmentDao;
import com.arsh.model.Assignment;
import com.arsh.model.Course;
import com.arsh.service.AssignmentService;

@Service
public class AssignmentServiceImpl implements AssignmentService{
	
	@Autowired
	private AssignmentDao assignmentDao;

	@Transactional
	public void saveAssignment(Assignment assignment) {
		assignment.setCreated(new Date());
		assignmentDao.saveAssignment(assignment);
		
	}

	@Transactional
	public void updateAssignment(Assignment assignment) {
		assignmentDao.updateAssignment(assignment);
		
	}

	@Transactional
	public List<Assignment> listAssignment() {
		return assignmentDao.listAssignment();
	}

	@Transactional
	public Assignment getAssignment(int id) {
		return assignmentDao.getAssignment(id);
	}

	@Transactional
	public List<Course> listCourses() {
		return assignmentDao.listCourses();
	}

	@Transactional
	public void deleteAssignment(int id) {
		assignmentDao.deleteAssignment(id);
		
	}

	@Transactional
	public List<Assignment> filterByCourse(int courseId) {
		 
		return assignmentDao.filterByCourse(courseId);
	}

	@Transactional
	public long getAssignmentCount() {
		 
		return assignmentDao.getAssignmentCount();
	}

}
