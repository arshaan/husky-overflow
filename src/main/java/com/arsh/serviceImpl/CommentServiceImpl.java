package com.arsh.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arsh.dao.CommentDao;
import com.arsh.model.Comment;
import com.arsh.service.CommentService;

@Service
public class CommentServiceImpl implements CommentService {

	@Autowired
	private CommentDao commentDao;

	@Transactional
	public void saveComment(Comment comment) {
		comment.setCreated(new Date());
		commentDao.saveComment(comment);
	}

	@Transactional
	public void updateComment(Comment comment) {
		commentDao.updateComment(comment);
	}

	@Transactional
	public List<Comment> listComments() {
		return commentDao.listComments();
	}

	@Transactional
	public Comment getComment(int id) {
		return commentDao.getComment(id);
	}

	@Transactional
	public void deleteComment(int id) {
		commentDao.deleteComment(id);

	}

	@Transactional
	public List<Comment> listCommentsByQuestion(int questionId) {
		return commentDao.listCommentsByQuestion(questionId);
	}

	@Transactional
	public long getCommentCount() {
		 
		return commentDao.getCommentCount();
	}



}
