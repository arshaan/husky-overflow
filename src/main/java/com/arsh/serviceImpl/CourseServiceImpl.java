package com.arsh.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arsh.dao.CourseDao;
import com.arsh.model.Course;
import com.arsh.model.Department;
import com.arsh.service.CourseService;
//Indicates that the annotated class (UserServiceImpl) is a "Service"
@Service
public class CourseServiceImpl implements CourseService{
	
	@Autowired
	private CourseDao courseDao;

	@Transactional
	public void saveCourse(Course course) {
		course.setCreated(new Date());
		courseDao.saveCourse(course);
		
	}

	@Transactional
	public void updateCourse(Course course) {
		courseDao.updateCourse(course);
		
	}

	@Transactional
	public List<Course> listCourses() {
		return courseDao.listCourses();
	}

	@Transactional
	public Course getCourse(int id) {
		
		return courseDao.getCourse(id);
	}

	@Transactional
	public void deleteCourse(int id) {
		courseDao.deleteCourse(id);
		
	}

	@Transactional
	public Department getDepartment(int id) {
		return courseDao.getDepartment(id);
	}

	@Transactional
	public List<Department> listDepartments() {
		return courseDao.listDepartments();
	}

	@Transactional
	public long getCourseCount() {
		 return courseDao.getCourseCount();
	}

	@Transactional
	public List<Course> filterByDepartment(int deptId) {
		return courseDao.filterByDepartment(deptId);
	}

	

}
