package com.arsh.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;



import org.springframework.transaction.annotation.Transactional;

import com.arsh.dao.DepartmentDao;
import com.arsh.model.Department;
import com.arsh.service.DepartmentService;
@Service
public class DepartmentServiceImpl implements DepartmentService{
	
	@Autowired
	private DepartmentDao departmentDao;

	@Transactional
	public void saveDepartment(Department department) {
		department.setCreated(new Date()); 
		departmentDao.saveDepartment(department);
		
	}

	@Transactional
	public void updateDepartment(Department department) {
		departmentDao.updateDepartment(department);
		
	}

	@Transactional
	public List<Department> listDepartments() {
		 return departmentDao.listDepartments();
	}

	@Transactional
	public Department getDepartment(int id) {
		
		return departmentDao.getDepartment(id);
	}

	@Transactional
	public void deleteDepartment(int id) {
		departmentDao.deleteDepartment(id);
		
	}

	@Transactional
	public long getDepartmentCount() {
		 
		return departmentDao.getDepartmentCount();
	}

}
