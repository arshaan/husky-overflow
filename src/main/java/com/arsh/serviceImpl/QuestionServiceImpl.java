package com.arsh.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arsh.dao.QuestionDao;
import com.arsh.model.Assignment;
import com.arsh.model.Question;
import com.arsh.service.QuestionService;

@Service
public class QuestionServiceImpl implements QuestionService {

	@Autowired
	private QuestionDao questionDao;

	@Transactional
	public void saveQuestion(Question question) {
		question.setCreated(new Date());
		questionDao.saveQuestion(question);

	}

	@Transactional
	public void updateQuestion(Question question) {
		questionDao.updateQuestion(question);

	}

	@Transactional
	public List<Question> listQuestions() {
			return questionDao.listQuestions();
	}

	@Transactional
	public Question getQuestion(int id) {

		return questionDao.getQuestion(id);
	}

	@Transactional
	public List<Assignment> listAssignments() {

		return questionDao.listAssignments();
	}

	@Transactional
	public void deleteQuestion(int id) {
		questionDao.deleteQuestion(id);

	}

	@Transactional
	public List<Question> listQuestionsByAssignmentId(int assignmentId) {
		 return questionDao.listQuestionsByAssignmentId(assignmentId);
	}

	@Transactional
	public long getQuestionCount() {
		 
		return questionDao.getQuestionCount();
	}

}
