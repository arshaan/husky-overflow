package com.arsh.serviceImpl;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.arsh.dao.UserDao;
import com.arsh.model.Department;
import com.arsh.model.User;
import com.arsh.service.UserService;

//Indicates that the annotated class (UserServiceImpl) is a "Service"
@Service
public class UserServiceImpl implements  UserService{
	
	@Autowired
	private UserDao userDao;
	
/*	Enables Spring's transactional behavior
	Enabled by putting <tx:annotation-driven/> in the context configuration file.
*/
	
	@Transactional
	public void saveUser(User user) {
		//set user enabled by default. Enable email verification later.
		//user.setEnabled(true);
		user.setCreated(new Date());
		userDao.saveUser(user);
		
	}

	@Transactional (readOnly = true)
	public List<User> listUsers() {
		 return userDao.listUsers();
	}

	@Transactional (readOnly = true)
	public User getUser(Long id) {
		return userDao.getUser(id);
	}

	@Transactional
	public void deleteUser(Long id) {
		userDao.deleteUser(id);
		
	}

	@Transactional
	public void updateUser(User user) {
		userDao.updateUser(user);
		
	}

	@Transactional
	public List<Department> listDepartments() {
		return userDao.listDepartments();
	}

	@Transactional
	public User findUserByUsername(String username) {
		 
		return userDao.findUserByUsername(username);
	}

	@Transactional
	public long getUserCount() {
		return userDao.getUserCount();
	}
	
	

}
