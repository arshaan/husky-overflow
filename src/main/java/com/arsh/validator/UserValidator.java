package com.arsh.validator;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

import com.arsh.model.User;

public class UserValidator implements Validator{
	@Override
	public boolean supports(Class<?> clazz) {
		
		return User.class.equals(clazz);
	}

	@Override
	public void validate(Object target, Errors errors) {
		// TODO Auto-generated method stub
		User user = (User)target;
		//password check
		if(user.getPassword().length()<6 || user.getPassword().length()>10){
			errors.rejectValue("password", "password[invalidLength]", "Password must be between 6 to 10 characters");
		}
		String huskyEmail = user.getUsername().trim();
		String neuDomain = huskyEmail.substring(huskyEmail.length() - 14);
		
		if(!neuDomain.toString().equals("@husky.neu.edu")){
			errors.rejectValue("username","username.invalid", "Please enter your husky email");
	
		}
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "department", "validate.department", "Your department Is Incorrect");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "name", "validate.name", "Your Name Is Incorrect");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "username", "validate.username", "Your Username Is Incorrect");
		ValidationUtils.rejectIfEmptyOrWhitespace(errors, "password", "validate.password", "Your password Is Incorrect");
	}

}
