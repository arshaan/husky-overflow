<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>Assignment Buddy - Access Denied</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="elements/user-anonymous-header.jsp"></c:import>
		<div class="text-center">
			<br/>
			<img src="${pageContext.request.contextPath}/resources/img/404.jpg" title="error-404" />  
			<h1>Error 404. Page Not Found!</h1> 
			<br/><a href="${pageContext.request.contextPath}/index" class="btn btn-large btn-primary">Go Home Husky</a>
		</div>
		
	</div>
	<!-- /container -->
	<c:import url="elements/footer.jsp"></c:import>
</body>
</html>