<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Administrator Dashboard</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="../elements/admin-header.jsp"></c:import>
		<div class="jumbotron">
			<h1>Welcome to { } HuskyOverflow Administrator</h1>
			<p>Assignment help forum developed with love by a husky for
				fellow huskies!</p>
			<p>
				
			</p>
		</div>
		

	</div>
	<c:import url="../elements/footer.jsp"></c:import>
</body>
</html>