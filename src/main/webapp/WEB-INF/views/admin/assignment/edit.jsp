<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Edit Course</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="../../elements/admin-header.jsp"></c:import>
		<div class="page-header">
			<h1>Edit Assignment</h1>
		</div>

		<form:form method="POST" action="../edit" commandName="assignment">
			<div class="form-group">
				<label>Select Course</label>
				<form:select path="course.courseId" class="form-control">

					<c:forEach items="${ListCourses}" var="course">
						<form:option value="${course.courseId}" label="${course.name}" />
					</c:forEach>
				</form:select>
			</div>
			<div class="form-group">
				<form:label path="title">
					<spring:message text="Title" />
				</form:label>
				<form:input path="title" class="form-control"
					placeholder="Enter Assignment Title" required="required"
					autofocus="true" />
			</div>

			<div class="form-group">
				<form:label path="description">
					<spring:message text="Description" />
				</form:label>
				<form:textarea path="description" class="form-control"
					placeholder="Enter Assignment Task" required="required"
					autofocus="true" />
			</div>
			<form:hidden path="assignmentId" value="${assignment.assignmentId}" />
			<button type="submit" class="btn btn-primary">Submit</button>

		</form:form>
	</div>
	<c:import url="../../elements/footer.jsp"></c:import>
</body>
</html>