<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Assignment List</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="../../elements/admin-header.jsp"></c:import>
		<div class="page-header">
			<h1>List of Assignments</h1>
		</div>
		<table class="table table-striped table-bordered  table-condensed">
			<thead>
				<tr>
					<td>ID</td>
					<td>Course</td>
					<td>Title</td>
					<td>Description</td>
					<td>Created On</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="assignment" items="${listAssignments}">
					<tr>
						<td>${assignment.assignmentId}</td>
						<td>${assignment.course.name}</td>
						<td>${assignment.title}</td>
						<td>${assignment.description}</td>
						<td>${assignment.created}</td>
						<td>
						
						<a href="../question/add/${assignment.assignmentId}"
							class="btn btn-small btn-primary">Add Question</a>
						
						<a href="../assignment/edit/${assignment.assignmentId}"
							class="btn btn-small btn-warning">Edit</a>
							
							 <a href="../assignment/delete/${assignment.assignmentId}"
							class="btn btn-small btn-danger">Delete</a>
						</td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<c:import url="../../elements/footer.jsp"></c:import>
</body>
</html>