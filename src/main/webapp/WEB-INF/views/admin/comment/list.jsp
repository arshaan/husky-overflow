<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Comment List</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="../../elements/admin-header.jsp"></c:import>
		<div class="page-header">
			<h1>List of Comments</h1>
		</div>
		<table class="table table-striped table-bordered  table-condensed">
			<thead>
				<tr>
					<td>Comment ID</td>
					<td>Course</td>
					<td>Assignment</td>
					<td>Question</td>
					<td>Asked By</td>
					<td>Commented By</td>
					<td>Comment</td>
					<td>Created On</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="comment" items="${listComments}">
					<tr>
						<td>${comment.commentId}</td>
						<td>${comment.question.assignment.course.name}</td>
						<td>${comment.question.assignment.title}</td>
						<td>${comment.question.question}</td>
						<td>${comment.question.user.name}</td>
						<td>${comment.user.name}</td>
						<td>${comment.comment}</td>
						<td>${comment.created}</td>

						<td> <a
							href="../comment/delete/${comment.commentId}"
							class="btn btn-small btn-danger">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>

	</div>
	<c:import url="../../elements/footer.jsp"></c:import>
</body>
</html>