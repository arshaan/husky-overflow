<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Users List</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="../../elements/admin-header.jsp"></c:import>
		<div class="page-header">
			<h1>List of Courses</h1>
		</div>
	<table class="table table-striped table-bordered">
			<thead>
				<tr>
					<td>Course ID</td>
					<td>Name</td>
					<td>Description</td>
					<td>Department</td>
					<td>Professor</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="course" items="${listCourses}">
					<tr>
						<td>${course.courseId}</td>
						<td>${course.name}</td>
						<td>${course.description}</td>
						<td>${course.department.department}</td>
						<td>${course.professor}</td>
						<td><a href="../course/edit/${course.courseId}" class="btn btn-small btn-warning">Edit</a> 
						<a href="../course/delete/${course.courseId}" class="btn btn-small btn-danger">Delete</a></td>
					</tr>
				</c:forEach>

			</tbody>
		</table>
	</div>
	<c:import url="../../elements/footer.jsp"></c:import>
</body>
</html>