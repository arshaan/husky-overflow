<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
    pageEncoding="ISO-8859-1"%>
     <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
      <%@ taglib uri="http://www.springframework.org/tags/form" prefix = "form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1">
<title>Add new course</title>
</head>
<body>

  <form:form  method="POST" action="../add?DEPT_ID=${deptID}" commandName="course">
  

    <table>
    
    
  
    <tr>
        <td>
            <form:label path="name">
                <spring:message text="Name"/>
            </form:label>
        </td>
        <td>
            <form:input path="name" />
        </td> 
    </tr>
    
    <tr>
        <td>
            <form:label path="description">
                <spring:message text="description"/>
            </form:label>
        </td>
        <td>
            <form:input path="description" />
        </td> 
    </tr>
   
    <tr>
        <td>
            <form:label path="professor">
                <spring:message text="professor"/>
            </form:label>
        </td>
        <td>
            <form:input path="professor" />
        </td> 
    </tr>
    
	
	    <tr>
             <td><input type = "submit" name="submit" value="Submit"/></td>
         </tr>
    </table>
			
    </form:form>  

</body>
</html>