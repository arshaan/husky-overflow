<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Add Department</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="../../elements/admin-header.jsp"></c:import>
		<div class="page-header">
			<h1>Add Department</h1>
		</div>

		<form:form method="POST" action="${pageContext.request.contextPath}/admin/department/add"
			commandName="department">
			<div class="form-group">
			<form:label path="department">
				<spring:message text="Name" />
			</form:label>

			<form:input path="department" class="form-control" placeholder="Enter Department Name"
				required="required" autofocus="true"/>
</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		
		</form:form>
	</div>
	<c:import url="../../elements/footer.jsp"></c:import>
</body>
</html>