<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Add User</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="../../elements/admin-header.jsp"></c:import>
		<div class="page-header">
			<h1>Add User</h1>
		</div>
		<form:form method="POST" action="${pageContext.request.contextPath}/admin/user/add" commandName="user">
			<div class="form-group">
				<form:label path="name">
					<spring:message text="Name" />
				</form:label>

				<form:input path="name" class="form-control" placeholder="Your Name"
				required="required" autofocus="true"/>
			</div>
			<div class="form-group">
				<form:label path="username">
					<spring:message text="Email as username" />
				</form:label>

				<form:input path="username" type="email" class="form-control" placeholder="Full Name"
				required="required" autofocus="true"/>
			</div>
			<div class="form-group">
				<form:label path="password">
					<spring:message text="Password" />
				</form:label>

				<form:password path="password" class="form-control"  placeholder="Enter password between 6 to 10 characters"
				required="required" autofocus="true"/>
			</div>
			<div class="form-group">
				<form:label path="department">
					<spring:message text="Select Department" />
				</form:label>
				<form:select path="department.deptId" class="form-control">
					<c:forEach items="${departments}" var="dep">
						<form:option value="${dep.deptId}" label="${dep.department}" />
					</c:forEach>
				</form:select>
			</div>
			<!-- Role -->
			<p><strong>Role</strong></p>
			<div class="radio">
				<label> <form:radiobutton path="role" name="role" id="role"
						value="ROLE_ADMIN" /> Administrator
				</label>

			</div>
			<div class="radio">
				<label > <form:radiobutton path="role" name="role" id="role"
						value="ROLE_USER" checked="checked"/> Student
				</label>

			</div>
			<button type="submit" class="btn btn-primary">Submit</button>
		</form:form>
	</div>
	<c:import url="../../elements/footer.jsp"></c:import>
</body>
</html>