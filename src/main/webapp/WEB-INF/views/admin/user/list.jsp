<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Users List</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="../../elements/admin-header.jsp"></c:import>
		<div class="page-header">
  <h1>List of Users</h1>
</div>

		<table class="table table-striped table-bordered table-condensed">
			<thead>
				<tr>
					<td>User ID</td>
					<td>Name</td>
					<td>Username - Email</td>
					<td>Role</td>
					<td>Department</td>
					<td>Action</td>
				</tr>
			</thead>
			<tbody>
				<c:forEach var="user" items="${listUsers}">
					<tr>
						<td>${user.userId}</td>
						<td>${user.name}</td>
						<td>${user.username}</td>
						
						<td>
						<c:if test="${user.role == 'ROLE_ADMIN'}">
							<c:out value="Administrator"></c:out> 
						</c:if>
						<c:if test="${user.role == 'ROLE_USER'}">
							<c:out value="Student"></c:out> 
						</c:if></td>
						<td>${user.department.department}</td>
						<td ><a href="../user/edit/${user.userId}" class="btn btn-small btn-warning">Edit</a>
						 <a href="../user/delete/${user.userId}" class="btn btn-small btn-danger">Delete</a></td>
					</tr>
				</c:forEach>
			</tbody>
		</table>
	</div>
	<c:import url="../../elements/footer.jsp"></c:import>
</body>
</html>