<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>Husky Overflow - Browse Assignments</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='resources/css/custom.css'/>" rel="stylesheet">
<link href="<c:url value='resources/css/chosen.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:if test="${not empty loggedUser.name}">
			<c:import url="elements/user-header.jsp"></c:import>
		</c:if>
		<c:if test="${empty loggedUser.name}">
			<c:import url="elements/user-anonymous-header.jsp"></c:import>
		</c:if>

		<h1>
			Browse Assignments <span class="text-right pull-right"><a
				href="${pageContext.request.contextPath}/user/assignment/add"
				class="btn btn-success">Add Assignment</a></span>
		</h1>



		<hr />
		<form:form method="POST" class="form-inline"
			action="${pageContext.request.contextPath}/browse"
			commandName="browseAssignment">
			<!-- Select department -->
			<div class="form-group">

				<form:select path="deptId" class="form-control chosen-select-department">
					<form:option value="0" label=" --- Select your department --- "></form:option>
					<form:options items="${listDepartments}" itemValue="deptId"
						itemLabel="department"></form:options>

				</form:select>
			</div>
			<!-- Select course-->
			<div class="form-group">

				<form:select path="courseId" id="courseId"
					class="form-control chosen-select-course">
					<form:option value="0" label=" --- Select your Course --- "></form:option>
					<%-- <form:option value="">Course</form:option> --%>
					<%-- <form:options items="${listCourses}" itemValue="courseId"
						itemLabel="name"></form:options> --%>

				</form:select>
			</div>
			<button type="submit" class="btn btn-warning">Load
				Assignments</button>

		</form:form>
		<br />
		<!-- show assignments -->
		<c:if test="${not empty listAssignments}">
			<table class="table table-striped table-bordered  table-condensed">
				<thead>
					<tr>
						<td>Date</td>
						<td>Department</td>
						<td>Course</td>
						<td>Title</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>


					<c:forEach var="assignment" items="${listAssignments}">
						<tr>
							<td>${assignment.created}</td>
							<td>${assignment.course.department.department}</td>
							<td>${assignment.course.name}</td>
							<td>${assignment.title}</td>
							<td><a href="browse/assignment/${assignment.assignmentId}"
								class="btn btn-small btn-primary">View</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>
	</div>
	<!-- /container -->
	<c:import url="elements/footer.jsp"></c:import>
	<!-- path to fetch courses for selected department via ajax  -->
	<c:url var="fetchCoursesForDepartmentURL"
		value="/fetchCoursesForDepartment" />
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script>
	<script>
		$(document).ready(function() { 
			/* Chosen invokes */
			$('.chosen-select-department').chosen(); 
			$('.chosen-select-course').chosen(); 
			
			/* Ajax call for courses */
			$('#deptId').chosen().change(
					 
										
			function() {
				$.getJSON('${fetchCoursesForDepartmentURL}', {
					deptId : $(this).val(),
					ajax : 'true',
				}, function(data) {
					var len = data.length;
					//clear the list
					$('.chosen-select-course').empty();
					for ( var i = 0; i < len; i++) {
						$('.chosen-select-course').append('<option value="'+data[i].courseId+'">'+ data[i].name + '</option>');
						$('.chosen-select-course').trigger("chosen:updated");
					}
					
					
				});
			});
			 
			
		});
	</script>
</body>
</html>

