<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>Husky Overflow - Assignment Details - ${assignment.title}</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:if test="${not empty loggedUser.name}">
			<c:import url="../elements/user-header.jsp"></c:import>
		</c:if>
		<c:if test="${empty loggedUser.name}">
			<c:import url="../elements/user-anonymous-header.jsp"></c:import>
		</c:if>
		<h3>${assignment.title}
			<span class="text-right pull-right"><a
				href="${pageContext.request.contextPath}/user/question/add/${assignment.assignmentId}"
				class="btn btn-success">Post New Question</a></span>
		</h3>
		<hr />
		<table class="table table-bordered table-striped">

			<thead>
				<tr>
					<td>Date Added</td>
					<td>Department</td>
					<td>Course</td>
					<td>Professor</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><fmt:formatDate
						value="${assignment.created}" pattern="MM-dd-yyyy" /></td>
					<td>${assignment.course.department.department}</td>
					<td>${assignment.course.name}</td>
					<td>${assignment.course.professor}</td>
				</tr>
			</tbody>
		</table>
		<h4>Description</h4>
		<p>${assignment.description}</p>


		<!-- list questions for this assignment -->
		<c:if test="${empty listQuestions}">
		<hr/>
			<h4>Sorry. No questions here! <a href="${pageContext.request.contextPath}/user/question/add/${assignment.assignmentId}">Post your question</a></h4>
		</c:if>
		<c:if test="${not empty listQuestions}">
			<table class="table table-striped table-bordered  table-condensed">
				<thead>
					<tr>
						<td>Date</td>
						<td>Asked By</td>
						<td>Question</td>
						<td>Action</td>
					</tr>
				</thead>
				<tbody>
					<c:forEach var="question" items="${listQuestions}">
						<tr>
							<td><fmt:formatDate
						value="${question.created}" pattern="MM-dd-yyyy" /></td>
							<td>${question.user.name}</td>
							<td>${question.question}</td>
							<td><a
								href="${pageContext.request.contextPath}/browse/comments?assignmentId=${assignment.assignmentId}&questionId=${question.questionId}"
								class="btn btn-small btn-primary">View Comments</a></td>
						</tr>
					</c:forEach>
				</tbody>
			</table>
		</c:if>


	</div>
	<!-- /container -->
	<c:import url="../elements/footer.jsp"></c:import>
</body>
</html>