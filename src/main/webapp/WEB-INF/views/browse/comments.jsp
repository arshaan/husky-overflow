<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>Husky Overflow - Comments</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:if test="${not empty loggedUser.name}">
			<c:import url="../elements/user-header.jsp"></c:import>
		</c:if>
		<c:if test="${empty loggedUser.name}">
			<c:import url="../elements/user-anonymous-header.jsp"></c:import>
		</c:if>
		<h3>${assignment.title}</h3>
		<table class="table table-bordered table-striped">
			<colgroup>
				<col class="col-md-1">
				<col class="col-md-2">
				<col class="col-md-2">
				<col class="col-md-2">
				<col class="col-md-5">
			</colgroup>
			<thead>
				<tr>
					<td>Date Added</td>
					<td>Department</td>
					<td>Course</td>
					<td>Professor</td>
					<td>Description</td>
				</tr>
			</thead>
			<tbody>
				<tr>
					<td><fmt:formatDate value="${assignment.created}"
							pattern="MM-dd-yyyy" /></td>
					<td>${assignment.course.department.department}</td>
					<td>${assignment.course.name}</td>
					<td>${assignment.course.professor}</td>
					<td>${assignment.description}</td>
				</tr>
			</tbody>
		</table>

		<div class="media">
			<div class="media-left">
				<img alt="${question.user.name} profile picture" title="${question.user.name}" class="thumbnail"
					src="${pageContext.request.contextPath}/profile/image/${question.user.userId}"
					width=70 onerror="this.src='${pageContext.request.contextPath}/resources/img/no-user-image.gif'"/>
					
				 
			</div>
			<div class="media-body">

				<h3 class="question-heading">${question.question}</h3>
				<small class="text-muted">- ${question.user.name} on <fmt:formatDate
						value="${question.created}" pattern="MM-dd-yyyy" /></small>

			</div>
		</div>
		<hr/>
		<!-- Ui mod -->
		<c:if test="${empty listComments}">
		
			<h4>Sorry. No comments here! <a href="${pageContext.request.contextPath}/user/comment/add/${question.questionId}">Be the first to comment</a></h4>
		</c:if>
		<c:forEach var="comment" items="${listComments}">
			<div class="media">
				<div class="media-left">
					<img alt="${comment.question.user.name} profile picture" title="${comment.user.name}" class="thumbnail"
					src="${pageContext.request.contextPath}/profile/image/${comment.user.userId}"
					width=70  onerror="this.src='${pageContext.request.contextPath}/resources/img/no-user-image.gif'"/>
				</div>
				<div class="media-body">
				<div class="row">
					<div class="col-md-10">
						<p>${comment.comment}</p>
					<small class="text-muted">- ${comment.user.name}
						on <fmt:formatDate value="${comment.created}" pattern="MM-dd-yyyy" />
					</small>
					</div>
					<div class="col-md-2">
							<a id="spam" commentId="${comment.commentId}" questionId="${question.questionId}" class="btn btn-danger">Spam</a>
							<a href="#" class="btn btn-primary">Like</a>
					</div>
				</div>
					
				</div>
			</div>
</c:forEach>
<br/>
<a class="btn btn-primary" href="${pageContext.request.contextPath}/user/comment/add/${question.questionId}">Post your comment</a>
	</div>
	<!-- /container -->
	<c:import url="../elements/footer.jsp"></c:import>
	<script>
	$(document).ready(function() { 
		
	/* Ajax call for courses */
	$('#spam').click(function() {
		var commentId = $(this).attr('commentId');
		var questionId = $(this).attr('questionId');
		 
		$.ajax({  
		    type: "POST",  
		    url: "${pageContext.request.contextPath}/comment/report-spam/"+commentId + "/" + questionId, 
			success: function(response){
		      if(response.status == "SUCCESS"){
		          alert("SUCCESS");
		      }else{
		            alert("FAILURE");
		      }       
		    },  
		    error: function(e){  
		      alert('Error: ' + e);  
		    }  
		  });  
		});
	});
	</script>
</body>
</html>