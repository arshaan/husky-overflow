<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>Assignment Buddy - Access Denied</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<nav class="navbar navbar-default navbar-fixed-top">
			<div class="container">
				<div class="navbar-header">
					<a class="navbar-brand" href="#">{ } HuskyOverflow</a>
				</div>
			</div>
		</nav>

		<c:if test="${not empty error}">
			<div class="panel panel-danger">
				<div class="panel-heading">
					<h3 class="panel-title" id="panel-title">
						Access Denied<a class="anchorjs-link" href="#panel-title"><span
							class="anchorjs-icon"></span></a>
					</h3>
				</div>
				<div class="panel-body">
					<p>Oops! You are unauthorized to access. Please login<p />
					<p> Caused : ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</p>
				</div>
			</div>
			<a href="${pageContext.request.contextPath}/login" class="btn btn-large btn-primary">Login</a>
		</c:if>

	</div>
	<!-- /container -->
	<c:import url="elements/footer.jsp"></c:import>
</body>
</html>