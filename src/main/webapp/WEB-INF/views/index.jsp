<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>

<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>Husky Overflow - Home</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:if test="${not empty loggedUser.name}">
			<c:import url="elements/user-header.jsp"></c:import>
		</c:if>
		<c:if test="${empty loggedUser.name}">
			<c:import url="elements/user-anonymous-header.jsp"></c:import>
		</c:if>
		<div class="jumbotron">
			<h1>Welcome to { } HuskyOverflow.</h1>
			<p>Assignment discussion forum developed with love by a husky for
				fellow huskies!</p>
			<p>
				<strong><a href="register">Sign up</a></strong> to post your
				questions and contribute.
			</p>
			<p>
				<a class="btn btn-primary btn-lg" href="browse" role="button">Browse
					Assignment Discussions</a>
			</p>
		</div>
		<h1>How are we doing?</h1>
		<hr />
		<div class="row">
			<div class="col-md-2">
				<h4>${userCount} Users</h4>
			</div>
			<div class="col-md-2">
				<h4>${deptCount} Departments</h4>
			</div>
			<div class="col-md-2">
				<h4>${courseCount} Courses</h4>
			</div>
			<div class="col-md-2">
				<h4>${assignmentCount} Assignments</h4>
			</div>
			<div class="col-md-2">
				<h4>${questionCount} Questions</h4>
			</div>
			<div class="col-md-2">
				<h4>${commentCount} Comments</h4>
			</div>
		</div>
	</div>
	<!-- /container -->
	<c:import url="elements/footer.jsp"></c:import>
</body>
</html>