<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>Assignment Buddy - Login</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="elements/user-anonymous-header.jsp"></c:import>
	<!-- error message -->
	<c:if test="${not empty error}">
	<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">
				<span aria-hidden="true">�</span>
			</button>
			<p><strong>WHOOPSIE!</strong> You are unauthorized </p>
			<small> ${sessionScope["SPRING_SECURITY_LAST_EXCEPTION"].message}</small> 
		</div>
	</c:if>
	<c:if test="${not empty validationSuccess}">
	<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">
				<span aria-hidden="true">�</span>
			</button>
			<p><strong>Yipee!</strong> Your email has been verified. Please login</p>
		</div>
	</c:if>
		<!--  login form-->

		<form class="form-signin" name="f" action="j_spring_security_check"
			method="post">
			<h2 class="form-signin-heading">Please Login</h2>
			<hr/>
			<label for="username" class="sr-only">Email address</label> <input
				type="email" name="j_username" id="username" class="form-control"
				placeholder="Email address" required autofocus> <label
				for="inputPassword" class="sr-only">Password</label> <input
				type="password" name="j_password" id="password" class="form-control"
				placeholder="Password" required>
			<div class="checkbox">
				<label> <input type="checkbox"
					name='_spring_security_remember_me'> Remember me
				</label>
			</div>
			<button class="btn btn-lg btn-primary btn-block" type="submit">Sign
				in</button><hr/>
				<a href="register" class="btn btn-warning">Sign Up</a>
		</form>
		
	</div>
	<!-- /container -->
	<c:import url="elements/footer.jsp"></c:import>
</body>
</html>