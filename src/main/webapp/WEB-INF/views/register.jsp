<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>

<!DOCTYPE html>
<html>
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Sign Up</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='resources/css/custom.css'/>" rel="stylesheet">
<link href="<c:url value='resources/css/chosen.css'/>" rel="stylesheet">

</head>
<body>
	<div class="container">
		<c:import url="elements/user-anonymous-header.jsp"></c:import>
		<c:if test="${not empty alert}">
			<div class="alert alert-danger alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<strong>Oops!</strong> ${alert} <a href="login">Please Login</a>
			</div>
		</c:if>
		<c:if test="${not empty regmailSent}">
			<div class="alert alert-success alert-dismissible" role="alert">
				<button type="button" class="close" data-dismiss="alert"
					aria-label="Close">
					<span aria-hidden="true">�</span>
				</button>
				<p>Thank you for signing up. Please check your husky mail for account activation instructions.</p>
			</div>
		</c:if>
		
		<form:form method="POST" class="form-signin" action="register"
			commandName="user">
			<h2 class="form-signin-heading">Sign Up</h2>
			<hr />
			<label for="name" >Name</label>
			<form:input path="name" class="form-control" placeholder="Your Name"
				required="required" autofocus="true" />
			<form:errors path="name" cssStyle="color:#ff0000"></form:errors>

			<label for="username" class="sr-only">Husky Email</label>
			<form:input path="username" class="form-control" type="email"
				placeholder="username@husky.neu.edu" required="required" autofocus="true" />
			<form:errors path="username" cssStyle="color:#ff0000"></form:errors> 
			<label for="department" class="sr-only">Department</label>
			<form:select path="department.deptId"
				class="form-control chosen-select-department">
				<form:option value="0" label=" ---- Select your department ---- "></form:option>
				<c:forEach items="${departments}" var="dep">
					<form:option value="${dep.deptId}" label="${dep.department}" />
				</c:forEach>
			</form:select>

			<label for="password" class="sr-only">Password</label>

			<form:password path="password" name="password" id="password"
				class="form-control" placeholder="Password" required="required" />
			<form:errors path="password" cssStyle="color:#ff0000"></form:errors>


			<button class="btn btn-lg btn-primary btn-block" type="submit">Submit</button>
			<br />
			<a href="login" class="btn btn-warning">Login</a>
		</form:form>

	</div>
	<!-- /container -->
	<c:import url="elements/footer.jsp"></c:import>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script>
	<script>
		$('.chosen-select-department').chosen();
	</script>
</body>
</html>