<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Add Assignment</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/chosen.css'/>" rel="stylesheet">

</head>
<body>
	<div class="container">
		<c:import url="../../elements/user-header.jsp"></c:import>
		<div class="page-header">
			<h1>Add Assignment</h1>
		</div>
<c:if test="${not empty message}">
		<div class="alert alert-success alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">
				<span aria-hidden="true">�</span>
			</button>
			<p>${message}</p> 
		</div>
		</c:if>
		<form:form method="POST"
			action="${pageContext.request.contextPath}/user/assignment/add"
			commandName="assignment">
			<div class="form-group">
				<label>Select Course</label>
				<form:select path="course.courseId" class="form-control chosen-select">
				<form:option value="0" label=" ---- Select a Course ---- "></form:option>
					
					<c:forEach items="${ListCourses}" var="course">
						<form:option value="${course.courseId}" label="${course.name}" />
					</c:forEach>
				</form:select>
			</div>

			<div class="form-group">
				<form:label path="title">
					<spring:message text="Title" />
				</form:label>
				<form:input path="title" class="form-control"
					placeholder="Enter Assignment Title" required="required"
					autofocus="true" />
			</div>

			<div class="form-group">
				<form:label path="description">
					<spring:message text="Description" />
				</form:label>
				<form:textarea rows="5" path="description" class="form-control"
					placeholder="Enter Assignment Task" required="required"
					autofocus="true" />
			</div>
			<button type="submit" class="btn btn-primary">Submit</button>

		</form:form>
	</div>
	<c:import url="../../elements/footer.jsp"></c:import>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script>
	<script>
		$('.chosen-select').chosen();
	</script>
</body>
</html>