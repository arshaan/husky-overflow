<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Edit User</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
<link href="<c:url value='/resources/css/chosen.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="../../elements/user-header.jsp"></c:import>
		<div class="page-header">
			<h1>Edit Profile</h1>
		</div>
		<c:if test="${not empty error}">
		<div class="alert alert-danger alert-dismissible" role="alert">
			<button type="button" class="close" data-dismiss="alert"
				aria-label="Close">
				<span aria-hidden="true">�</span>
			</button>
			<p><strong>WHOOPSIE!</strong> ${error} </p>
			 
		</div>
	</c:if>
		<form:form method="POST"
			action="${pageContext.request.contextPath}/user/profile/edit"
			commandName="loggedUser">
			<div class="form-group">
				<form:label path="name">
					<spring:message text="Name" />
				</form:label>
				<form:input path="name" class="form-control" />
			</div>
			<div class="form-group">
				<form:label path="username">
					<spring:message text="Email as username" />
				</form:label>

				<form:input path="username" class="form-control" />
			</div>
			<div class="form-group">
				<form:label path="department">
					<spring:message text="Select Department " />
				</form:label>
				<form:select path="department.deptId" class="form-control chosen-select">
			
					<c:forEach items="${departments}" var="dep">
						<form:option value="${dep.deptId}" label="${dep.department}" />
					</c:forEach>
				</form:select>
			</div>
			<div class="form-group">
				<form:label path="password">
					<spring:message text="Password" />
				</form:label>

				<form:password path="password" class="form-control" />
			</div>
			<!-- Role -->


			<form:hidden path="userId" />
			<form:hidden path="avtar"/>

			<button type="submit" class="btn btn-primary">Submit</button>
		</form:form>
  		<h3>Upload profile picture- <small class="text-muted">Only Jpg files within 100kb allowed</small></h3>
  		
		<form method="POST" action="${pageContext.request.contextPath}/user/profile/uploadProfilePic"
			enctype="multipart/form-data">
			
			File to upload: <input type="file" name="file"><br />
			<button type="submit" class="btn btn-primary">Submit</button>
		</form>  
	</div>
	<c:import url="../../elements/footer.jsp"></c:import>
	<script
		src="https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js"></script>
	<script src="http://harvesthq.github.io/chosen/chosen.jquery.js"></script>
	<script>
		$('.chosen-select').chosen();
	</script>
</body>
</html>