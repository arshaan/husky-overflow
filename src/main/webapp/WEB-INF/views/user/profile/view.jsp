<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<meta name="author" content="Arshaan Shariff">
<title>{ } HuskyOverflow - Edit User</title>

<!-- Bootstrap core CSS -->
<link href="<c:url value='/resources/css/bootstrap.min.css'/>"
	rel="stylesheet">
<!-- Custom styles -->
<link href="<c:url value='/resources/css/custom.css'/>" rel="stylesheet">
</head>
<body>
	<div class="container">
		<c:import url="../../elements/user-header.jsp"></c:import>
		<div class="page-header">
			<h1>My Profile</h1>
		</div>
		<div class="row">
			<div class="col-md-3">
				<img alt="${loggedUser.name} profile picture" title="${loggedUser.name}" class="thumbnail"
					src="${pageContext.request.contextPath}/profile/image/${loggedUser.userId}"
					width=200 onerror="this.src='${pageContext.request.contextPath}/resources/img/no-user-image.gif'"/>
			</div>
			<div class="col-md-9">
				<p>
					<strong>Name: </strong> ${loggedUser.name}
				</p>
				<p>
					<strong>Email:</strong> ${loggedUser.username}</p>
				<p>
					<strong>Department:</strong> ${loggedUser.department.department}</p>
				<p>
					
					<a href="${pageContext.request.contextPath}/user/profile/edit"
			class="btn btn-primary">Edit Profile</a>
			</div>
		</div>

	</div>
	<c:import url="../../elements/footer.jsp"></c:import>
</body>
</html>